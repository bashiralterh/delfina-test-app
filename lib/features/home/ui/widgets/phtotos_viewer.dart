import 'package:flutter/cupertino.dart';
import 'package:new_app/core/common_widgets/animation_scale_widget.dart';
import 'package:new_app/core/utils/enums.dart';

import '../../../../core/common_widgets/image_view.dart';
import '../../../../core/constants/app_colors.dart';
import '../../logic/home_BLoC.dart';
import '../../models/photo_model.dart';

class PhotosViewer extends StatefulWidget {
  const PhotosViewer({Key? key}) : super(key: key);

  @override
  _PhotosViewerState createState() => _PhotosViewerState();
}

class _PhotosViewerState extends State<PhotosViewer> {
  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    /**
     * I use StreamBuilder to listen once data updated from BLoC layer
     */
    return StreamBuilder<PhotoModel>(
        stream: homeBloc.photoStream,
        builder: (context, snapshot) {
          return StreamBuilder<PlayType>(
              stream: homeBloc.playTypeStream,
              initialData: PlayType.pause,
              builder: (context, snapshotPlayType) {
                return AnimationScaleWidget(
                  horizontalOffset: 0,
                  verticalOffset: -100,
                  child: Container(
                    width: _screenSize.width * 0.7,
                    height: _screenSize.height * 0.35,
                    decoration: BoxDecoration(
                      border: Border.all(color: _getColor(snapshotPlayType.data), width: 2),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: (snapshot.hasData)
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: ImageView(
                              imageUrl: snapshot.data?.urls?.small ?? '',
                            ),
                          )
                        : Icon(CupertinoIcons.photo_on_rectangle, size: 150, color: AppColors.gray),
                  ),
                );
              });
        });
  }


  /**
   * There is a unique color for each type (Play,Pause and Rewind)
   */
  Color _getColor(PlayType? type) {
    switch (type ?? PlayType.pause) {
      case PlayType.play:
        return AppColors.green;
      case PlayType.pause:
        return AppColors.red;
      case PlayType.rewind:
        return AppColors.blue;
    }
  }
}
