import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:new_app/core/common_widgets/animation_scale_widget.dart';
import 'package:new_app/core/constants/app_colors.dart';
import 'package:new_app/core/constants/app_constants.dart';
import 'package:new_app/core/utils/enums.dart';
import 'package:new_app/features/home/logic/home_BLoC.dart';
import 'package:new_app/features/home/ui/widgets/phtotos_viewer.dart';

import '../../../core/common_widgets/button_view.dart';
import '../../../core/constants/app_images.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Padding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + 8),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _titleWidget(),
                SizedBox(
                  height: 32,
                ),
                PhotosViewer(),
                SizedBox(
                  height: 24,
                ),
                _controlButtons()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _titleWidget() {
    return Text(
      '${AppConstants.appName}',
      style: Theme.of(context).textTheme.headline5?.copyWith(color: AppColors.black),
    );
  }

  Widget _controlButtons() {
    Size _screenSize = MediaQuery.of(context).size;

    /**
     * I use StreamBuilder to listen once data updated from BLoC layer
     */
    return StreamBuilder<PlayType>(
        stream: homeBloc.playTypeStream,
        initialData: PlayType.pause,
        builder: (context, snapshot) {
          return AnimationScaleWidget(
            verticalOffset: -50,
            child: SizedBox(
              width: _screenSize.width * 0.7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: AnimationScaleWidget(
                      verticalOffset: 0,
                      horizontalOffset: -50,
                      child: ButtonView(
                        imagePath: AppImages.rewind,
                        text: 'rewind'.tr(),
                        color: AppColors.blue,
                        onPressed: () {
                          homeBloc.setPlayType(context, PlayType.rewind);
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: AnimationScaleWidget(
                      verticalOffset: 0,
                      horizontalOffset: 50,
                      child: (snapshot.data == PlayType.pause)
                          ? ButtonView(
                              imagePath: AppImages.play,
                              text: 'play'.tr(),
                              color: AppColors.green,
                              onPressed: () {
                                homeBloc.setPlayType(context, PlayType.play);
                              },
                            )
                          : ButtonView(
                              imagePath: AppImages.pause,
                              text: 'pause'.tr(),
                              color: AppColors.red,
                              onPressed: () {
                                homeBloc.setPlayType(context, PlayType.pause);
                              },
                            ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  void dispose() {
    super.dispose();
    homeBloc.cancelTimer();
  }
}
