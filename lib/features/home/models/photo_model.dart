import 'dart:convert';

PhotoModel photoModelFromJson(String str) => PhotoModel.fromJson(json.decode(str));

String photoModelToJson(PhotoModel data) => json.encode(data.toJson());

class PhotoModel {
  PhotoModel({
    String? id,
    String? description,
    String? altDescription,
    Urls? urls,
  }) {
    _id = id;
    _description = description;
    _altDescription = altDescription;
    _urls = urls;
  }

  PhotoModel.fromJson(dynamic json) {
    _id = json['id'];
    _description = json['description'];
    _altDescription = json['alt_description'];
    _urls = json['urls'] != null ? Urls.fromJson(json['urls']) : null;
  }

  String? _id;
  String? _description;
  String? _altDescription;
  Urls? _urls;

  PhotoModel copyWith({
    String? id,
    String? description,
    String? altDescription,
    Urls? urls,
  }) =>
      PhotoModel(
        id: id ?? _id,
        description: description ?? _description,
        altDescription: altDescription ?? _altDescription,
        urls: urls ?? _urls,
      );

  String? get id => _id;

  String? get description => _description;

  String? get altDescription => _altDescription;

  Urls? get urls => _urls;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['description'] = _description;
    map['alt_description'] = _altDescription;
    if (_urls != null) {
      map['urls'] = _urls?.toJson();
    }
    return map;
  }
}

Urls urlsFromJson(String str) => Urls.fromJson(json.decode(str));

String urlsToJson(Urls data) => json.encode(data.toJson());

class Urls {
  Urls({
    String? raw,
    String? full,
    String? regular,
    String? small,
    String? thumb,
    String? smallS3,
  }) {
    _raw = raw;
    _full = full;
    _regular = regular;
    _small = small;
    _thumb = thumb;
    _smallS3 = smallS3;
  }

  Urls.fromJson(dynamic json) {
    _raw = json['raw'];
    _full = json['full'];
    _regular = json['regular'];
    _small = json['small'];
    _thumb = json['thumb'];
    _smallS3 = json['small_s3'];
  }

  String? _raw;
  String? _full;
  String? _regular;
  String? _small;
  String? _thumb;
  String? _smallS3;

  Urls copyWith({
    String? raw,
    String? full,
    String? regular,
    String? small,
    String? thumb,
    String? smallS3,
  }) =>
      Urls(
        raw: raw ?? _raw,
        full: full ?? _full,
        regular: regular ?? _regular,
        small: small ?? _small,
        thumb: thumb ?? _thumb,
        smallS3: smallS3 ?? _smallS3,
      );

  String? get raw => _raw;

  String? get full => _full;

  String? get regular => _regular;

  String? get small => _small;

  String? get thumb => _thumb;

  String? get smallS3 => _smallS3;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['raw'] = _raw;
    map['full'] = _full;
    map['regular'] = _regular;
    map['small'] = _small;
    map['thumb'] = _thumb;
    map['small_s3'] = _smallS3;
    return map;
  }
}
