import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:new_app/core/utils/general_BLoC.dart';
import 'package:new_app/features/home/logic/home_apis.dart';
import 'package:new_app/features/home/models/photo_model.dart';
import 'package:rxdart/rxdart.dart';

import '../../../core/utils/enums.dart';
import '../../../core/utils/general_methods.dart';

class HomeBloc extends GeneralBLoC {
  final HomeApis _apis = HomeApis();

  /**
   * We use stream & controller to sink data
   */
  final _photoController = PublishSubject<PhotoModel>();

  Stream<PhotoModel> get photoStream => _photoController.stream;

  final _playTypeController = BehaviorSubject<PlayType>();

  Stream<PlayType> get playTypeStream => _playTypeController.stream;


  ///This list to save photo for rewinding
  List<PhotoModel?> _savedPhotoList = [];

  HomeBloc.internal() : super.internal();

  internal() {
    _playTypeController.sink.add(PlayType.pause);
  }


  getPhoto(BuildContext? context,
      {String query = 'dolphin', Function({PhotoModel? value, dynamic error})? onGetResult}) {
    ///Stop timer until get API response to avoid interruption
    _stopTimer(paused: false);
    fetchData(_apis.getPhoto(context, query), onData: (value) {
      if (onGetResult != null) onGetResult(value: value);
      _photoController.sink.add(value);
      _playPhotos(context, playWithTimer: true);

      GeneralHelperMethods.vibrate();

      _savePhoto(value);
    }, onError: (error) {
      if (onGetResult != null) onGetResult(error: error);
      _photoController.sink.addError(error);
      _stopTimer();
      _playTypeController.sink.add(PlayType.pause);
    });
  }

  ///Save photos in temporary list for rewinding
  _savePhoto(PhotoModel value) {
    ///Only save 5 photos
    if (_savedPhotoList.length == 5) _savedPhotoList.removeAt(0);
    _savedPhotoList.add(value);
  }

  Timer? timer;

  ///2 seconds
  int delayTime = 2;


  /**
   * Set type Play & Pause and Rewind
   */
  setPlayType(BuildContext? context, PlayType type) {
    _playTypeController.sink.add(type);

    switch (type) {
      case PlayType.play:
        _playPhotos(context);
        break;
      case PlayType.pause:
        _stopTimer();
        break;
      case PlayType.rewind:
        _rewindPhotos(context);
        break;
    }
  }

  /**
   * this method use to stop timer
   * 'paused' param check if you want to pause loading photos
   */
  _stopTimer({bool paused = true}) {
    timer?.cancel();
    if (paused) _playTypeController.sink.add(PlayType.pause);
  }

  /**
   * This method called once user press 'Play' button
   * To get new photo from the API
   * 'playWithTimer' param check if need to start timer or call API once
   */
  _playPhotos(BuildContext? context, {bool playWithTimer = false}) {
    if (playWithTimer) {
      timer = Timer.periodic(Duration(seconds: delayTime), (Timer t) {
        getPhoto(
          context,
        );
      });
    }
    else
      getPhoto(
        context,
      );
  }


  /**
   * This method use to show old 5 photos every 2 seconds
   */
  _rewindPhotos(BuildContext? context) {
    ///Check if there are photos or not
    if (_savedPhotoList.isEmpty) {
      _stopTimer();
      if (context != null) GeneralHelperMethods.showAlertDialog(context, title: 'no_saved_photos'.tr(), content: '');
      return;
    }

    _stopTimer(paused: false);
    int index = _savedPhotoList.length - 1;
    timer = Timer.periodic(Duration(seconds: delayTime), (Timer t) {
      GeneralHelperMethods.vibrate();
      ///No more photos to display
      if (index == -1) {
        GeneralHelperMethods.showAlertDialog(context, title: 'cannot_remember_any_more_dolphins'.tr(), content: '');
        _stopTimer();
        return;
      }

      _photoController.sink.add(_savedPhotoList[index] ?? PhotoModel());
      index--;
    });
  }

  cancelTimer() {
    timer?.cancel();
  }
}

HomeBloc homeBloc = HomeBloc.internal();
