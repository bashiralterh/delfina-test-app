import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:new_app/core/constants/app_constants.dart';

import '../../../core/constants/api_urls.dart';
import '../../../core/utils/api_provider.dart';
import '../models/photo_model.dart';

/**
 * This class used for get APIs response from ApiProvider
 */
class HomeApis {
  Future<PhotoModel?> getPhoto(BuildContext? context,String query,) async {
    var response = await ApiProvider.dataLoader(
      context,
      RequestType.get,
      '${ApiUrls.getRandomPhotos}?query=$query&client_id=${AppConstants.clientId}',
    );

    try {
      return PhotoModel.fromJson(json.decode(response.toString()));
    } catch (e) {
      return null;
    }
  }
}
