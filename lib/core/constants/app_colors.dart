import 'package:flutter/material.dart';

class AppColors {
  static const Color green = Color(0xFF77BE7E);
  static const Color black = Color(0xFF000000);
  static const Color blue = Color(0xFF0F6BAC);
  static const Color white = Color(0xFFFFFFFF);
  static const Color gray = Colors.grey;
  static const Color red = Color(0xFFBC2F2F);
}
