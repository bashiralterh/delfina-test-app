class ApiUrls {
  static const String baseUrl = 'https://api.unsplash.com/';
  static const String getRandomPhotos  = '${baseUrl}photos/random';
}
