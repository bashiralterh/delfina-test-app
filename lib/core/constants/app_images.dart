class AppImages {
  static const String logo = 'assets/images/logo.png';
  static const String pause = 'assets/images/ic_pause.svg';
  static const String play = 'assets/images/ic_play.svg';
  static const String rewind = 'assets/images/ic_rewind.svg';
}
