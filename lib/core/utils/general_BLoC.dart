import 'package:flutter/material.dart';

/**
  * This class contains the general & Base methods for BLoC
  * Each BLoC class must extends from this class
 */
class GeneralBLoC {
  GeneralBLoC.internal();


  /**
   * General method must used in all BLoC classes
   */
  fetchData(Future<dynamic>? func, {required void onData(dynamic data), required void onError(dynamic error)}) {
    func?.then((value) {
      debugPrint('fetchData done: $value');
      if (value == null)
        onError('Error');
      else
        onData(value);
    });
  }
}

final bloc = GeneralBLoC.internal();
