import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';


/**
 * This widget to add simple animation in simple way
 * Just make this widget the parent of the widget you want to apply animation with
 */
class AnimationScaleWidget extends StatelessWidget {
  final int position;
  final double horizontalOffset, verticalOffset;
  final Widget child;
  final int milliseconds;

  const AnimationScaleWidget({
    Key? key,
    this.position = 0,
    this.horizontalOffset = 0,
    this.verticalOffset = 10,
    required this.child,
    this.milliseconds = 500,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
      return AnimationConfiguration.staggeredList(
        position: position,
        duration: Duration(milliseconds: milliseconds),
        child: SlideAnimation(
          horizontalOffset: horizontalOffset,
          verticalOffset: verticalOffset,
          child: FadeInAnimation(
            child: child,
          ),
        ),
      );
  }
}
