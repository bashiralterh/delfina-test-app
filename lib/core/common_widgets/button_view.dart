import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:new_app/core/constants/app_colors.dart';

class ButtonView extends StatelessWidget {
  const ButtonView(
      {Key? key, required this.imagePath, required this.color, required this.text, required this.onPressed})
      : super(key: key);

  final String imagePath, text;
  final Color color;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
        onPressed: () {
          onPressed();
        },
        style: TextButton.styleFrom(
            backgroundColor: color.withOpacity(0.3),
            primary: AppColors.white,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
        icon: SvgPicture.asset(imagePath),
        label: Text(
          text,
          style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: color),
        ));
  }
}
