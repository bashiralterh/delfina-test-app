1-*To run app write 'flutter run' in terminal
  *Write 'flutter build apk --target-platform android-arm,android-arm64 --split-per-abi' to build APK
  *Write 'flutter test' to run test cases

2-There are three base layers:
 *BLoC layer to state management using 'rxdart'
 *API layer to get APIs response *UI layer for displaying
 *There are some helpers classes to make clean code and don't repeat any code twice

3-This project using Flutter 3.0.5 (Last version)

4-This project using BLoC statement management

5-I used 'https://unsplash.com/' to get APIs

6-All plugin here using the last version

7-The App have some animation

You can check the output from this link: https://drive.google.com/drive/folders/1n9FyngZMUHrgPTaYh635l2HGHTO6MvFw
