import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:new_app/features/home/logic/home_BLoC.dart';


@GenerateMocks([HomeBloc])
void main() {
  // HomeBloc? homeBloc;

  setUp(() {
    // homeBloc = MockHomeBloc();
  });

  test('get photo', () async {
    when(homeBloc.getPhoto(null, onGetResult: ({value, error}) {
      verify(value);
      return value;
    }));
  });

  // test('setPlayType', () async {
  //   when(homeBloc.setPlayType(null, PlayType.pause)).thenAnswer((realInvocation) {
  //     verify(realInvocation);
  //
  //     return realInvocation;
  //   });
  // });
}
